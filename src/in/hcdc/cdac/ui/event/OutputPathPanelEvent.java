/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hcdc.cdac.ui.event;

import com.lowagie.text.DocumentException;
import static in.hcdc.cdac.constants.Constants.CARD_DASHBOARD;
import in.hcdc.cdac.database.api.RecordAPI;
import in.hcdc.cdac.main.IDECardsHolder;
import in.hcdc.cdac.ui.OutputPathPanel;
import in.hcdc.cdac.util.GenerateReport;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public class OutputPathPanelEvent implements ActionListener {

    private final OutputPathPanel outputPathPanel;
    private final IDECardsHolder ideCardsHolder;
    private Component tablePanel, errorMsgpanel;

    public OutputPathPanelEvent(OutputPathPanel outputPathPanel, IDECardsHolder ideCardsHolder) {
        this.outputPathPanel = outputPathPanel;
        this.ideCardsHolder = ideCardsHolder;
    }

    @Override
    public void actionPerformed(ActionEvent e)  {
        RecordAPI recordAPI = null;
        String sourcePath = outputPathPanel.getTxtOutputPath().getText();
        String transferPath = outputPathPanel.getTxtTransferPath().getText();
        File source = new File(sourcePath);
        File dest = new File(transferPath);
        if (e.getSource() == outputPathPanel.getBtnSave()) {

            //  System.out.println(":: output path ::"+outputPath);
            sourcePath = sourcePath.replace('\\', '/');
            boolean flag = false;
            if (flag) {
                JOptionPane.showMessageDialog(outputPathPanel, "None of the fields can be empty or \n can have any of the following characters \\ | * : / ? < >");
                return;
            }
        } else if (e.getSource() == outputPathPanel.getBtnReset()) {
            outputPathPanel.getTxtOutputPath().setText("");
            outputPathPanel.getTxtOutputPath().setEnabled(true);
            outputPathPanel.getBtnBrowse().setEnabled(true);
            outputPathPanel.getBtnReset().setEnabled(false);
            outputPathPanel.getBtnSubmit().setEnabled(true);
            errorMsgpanel = outputPathPanel.getErrorMsgPanel();
            if (errorMsgpanel != null) {
                outputPathPanel.getContentPanel().remove(errorMsgpanel);
            }
            tablePanel = outputPathPanel.getTablepanel();
            if (tablePanel != null) {
                outputPathPanel.getContentPanel().remove(tablePanel);
            }
            outputPathPanel.repaint();
            outputPathPanel.validate();
            outputPathPanel.setVisible(true);
        }else if(e.getSource() == outputPathPanel.getBtntranseferReset()){
             outputPathPanel.getTxtTransferPath().setText("");
        }else if (e.getSource() == outputPathPanel.getBtnCancel()) {
            ideCardsHolder.getcLayout().show(ideCardsHolder, CARD_DASHBOARD);
        } else if (e.getSource() == outputPathPanel.getBtnSubmit()) {
            outputPathPanel.getBtnSubmit().setEnabled(false);
            outputPathPanel.getBtnReset().setEnabled(true);
            outputPathPanel.updateListTableModel();
        } else if (e.getSource() == outputPathPanel.getBtnMove()) {
            if (transferPath == null || transferPath.equals("")) {
                outputPathPanel.alertbox("Please select target folder");
                return;
            }
            JTable tableModel = outputPathPanel.getTableList();
            Map<Integer,String> map = new HashMap<Integer,String>();
            int count = tableModel.getRowCount();
            File recordFile;
            File recordFolder;
            int no_of_records_selected = 0;
            System.out.println("-- Transfer path --"+transferPath);
            for (int i = 0; i < count; i++) {
                Boolean chkDel = Boolean.valueOf(tableModel.getValueAt(i, 0).toString()); // Checked
                if (chkDel) {
                    no_of_records_selected++;
                    try {
                     //   recordsIdlist.add(tableModel.getValueAt(i, 1).toString());
                        
                        map.put((Integer) tableModel.getValueAt(i, 1), transferPath);
                        
                        recordFile = new File(tableModel.getValueAt(i, 2).toString());
                        recordFolder = new File(tableModel.getValueAt(i, 3).toString());
                        FileUtils.copyFile(new File(recordFolder.getParent() + "/" + recordFile), new File(transferPath + "/" + recordFile));
                        System.out.println("--Record folder--" + recordFolder.getAbsolutePath() + "\n" + recordFolder.getParent());
                                                
                      /*  String parentPath = recordFolder.getParent();
                        String recordFolderName = parentPath.substring(parentPath.lastIndexOf("\\") + 1, parentPath.length());
                        System.out.println("record Folder name::" + recordFolderName);
                        File file = new File(dest + "/" + recordFolderName);
                        if (!file.exists()) {
                            file.mkdir();
                            FileUtils.copyFile(new File(recordFolder.getParent() + "/" + recordFile), new File(file + "/" + recordFile));
                        } */
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
            if (no_of_records_selected == 0) {
                outputPathPanel.alertbox("Please select atleast one record to move");
                return;
            } else {
                /**s
                 * update to db after moving records to the target path
                 */
                try {
                    recordAPI = new RecordAPI();
                    int rowsaffected = recordAPI.updateFileTransferStatus(map);
                    if (rowsaffected > 0) {

                    }
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(OutputPathPanelEvent.class.getName()).log(Level.SEVERE, null, ex);
                }
                /*            } catch (IOException ex) {
                Logger.getLogger(OutputPathPanelEvent.class.getName()).log(Level.SEVERE, null, ex);
                } */
                outputPathPanel.alertbox(no_of_records_selected + " records moved successfully");
                outputPathPanel.getBtnReportDnd().setEnabled(true);
            }
        }
         else if (e.getSource() == outputPathPanel.getBtnReportDnd()) {
             GenerateReport report = new GenerateReport();
            try {
                report.createPdf();
            } catch (IOException ex) {
                Logger.getLogger(OutputPathPanelEvent.class.getName()).log(Level.SEVERE, null, ex);
            } catch (DocumentException ex) {
                Logger.getLogger(OutputPathPanelEvent.class.getName()).log(Level.SEVERE, null, ex);
            }            
         }
    }

    public List<Component> getAllComponents(final Container c) {
        Component[] comps = c.getComponents();
        List<Component> compList = new ArrayList<>();
        for (Component comp : comps) {
            compList.add(comp);
            if (comp instanceof Container) {
                compList.addAll(getAllComponents((Container) comp));
            }
        }
        return compList;
    }
}
