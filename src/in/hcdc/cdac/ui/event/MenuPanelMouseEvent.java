/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hcdc.cdac.ui.event;

import static in.hcdc.cdac.constants.Constants.*;
import in.hcdc.cdac.main.IDECardsHolder;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 * @version 1
 * @since 1
 */
public class MenuPanelMouseEvent extends MouseAdapter {

    private IDECardsHolder ideCardsHolder;

    public MenuPanelMouseEvent(IDECardsHolder ideCardsHolder) {
        this.ideCardsHolder = ideCardsHolder;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        JLabel check = (JLabel) e.getSource();
        if (check.getText().equals(ideCardsHolder.getMenuPanel().getLblBasicConfiguraion().getText())) {
            ideCardsHolder.getcLayout().show(ideCardsHolder, MENU_ITEM_BASIC_CONFIGURATION);
        }
    }
}
