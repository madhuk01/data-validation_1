/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hcdc.cdac.ui.event;

import static in.hcdc.cdac.constants.Constants.MENU_ITEM_BASIC_CONFIGURATION;
import static in.hcdc.cdac.constants.Constants.MENU_ITEM_EXIT;
import in.hcdc.cdac.logger.Logger;
import in.hcdc.cdac.main.IDECardsHolder;
import in.hcdc.cdac.main.IDECardsHolder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public class IDEEvent implements ActionListener {

    private IDECardsHolder ideCardsHolder;

    public IDEEvent(IDECardsHolder ideCardsHolder) {
        this.ideCardsHolder = ideCardsHolder;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("IDE Event--------->" + e.getActionCommand());
        switch (e.getActionCommand()) {
            case MENU_ITEM_BASIC_CONFIGURATION:
                Logger.getInstance().logMessage("Display Basic Configuration Settings.");
                ideCardsHolder.getcLayout().show(ideCardsHolder, MENU_ITEM_BASIC_CONFIGURATION);
                break;
            case MENU_ITEM_EXIT:
                int confirm = JOptionPane.showConfirmDialog(null, "Are you sure you want to exit?",
                        "Exit Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if (confirm == JOptionPane.YES_OPTION) {
                    System.exit(1);
                }
                break;
        }
    }
}
