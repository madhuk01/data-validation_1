package in.hcdc.cdac.ui;

import java.awt.Color;
import java.awt.Cursor;
import java.net.URL;
import javax.swing.*;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public class MenuPanel extends JPanel {

    private JLabel lblErecordPrepare, lblBasicConfiguraion;
    static JFrame f;

    public MenuPanel() {
//        this.setSize(700, 600);
        initComponents();
        layoutComponents();
    }

    private void layoutComponents() {
        MigLayout mig = new MigLayout("insets 0", "[]", "[]10[]10[]10[]10[]10[]10[]10[]10[]");
        setLayout(mig);

        URL imgURL = MenuPanel.class.getResource("/in/hcdc/cdac/resources/images/logo.png");
        JLabel top = new JLabel(new ImageIcon(imgURL));
        setBackground(Color.decode("#ccddee"));
        add(top, "north,wrap, height 15%!");

        //row 0
        add(lblErecordPrepare, "span 2, wrap,width 25%,height 25!");
        add(lblBasicConfiguraion, "span 2, wrap,width 25%,height 25!");

    }

//    public static void main(String[] ar) {
//        f = new JFrame();
//        f.setContentPane(new MenuPanel());
//        f.pack();
//        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        f.setVisible(true);
//    }
    private void initComponents() {
        URL imgURL = MenuPanel.class.getResource("/in/hcdc/cdac/resources/images/caption_prepare.png");
        lblErecordPrepare = new JLabel(new ImageIcon(imgURL));
        imgURL = MenuPanel.class.getResource("/in/hcdc/cdac/resources/images/db_connect.png");
        lblBasicConfiguraion = new JLabel(new ImageIcon(imgURL));
        lblBasicConfiguraion = new JLabel("Basic Configuration");
        lblBasicConfiguraion.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

    }

    public static JFrame getF() {
        return f;
    }

    public static void setF(JFrame f) {
        MenuPanel.f = f;
    }

    public JLabel getLblErecordPrepare() {
        return lblErecordPrepare;
    }

    public void setLblErecordPrepare(JLabel lblErecordPrepare) {
        this.lblErecordPrepare = lblErecordPrepare;
    }

    public JLabel getLblBasicConfiguraion() {
        return lblBasicConfiguraion;
    }

    public void setLblBasicConfiguraion(JLabel lblBasicConfiguraion) {
        this.lblBasicConfiguraion = lblBasicConfiguraion;
    }

}
