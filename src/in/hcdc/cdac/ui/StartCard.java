/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hcdc.cdac.ui;

import in.hcdc.cdac.main.IDECardsHolder;
import in.hcdc.cdac.ui.event.MenuPanelMouseEvent;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public class StartCard extends JPanel {

    private JPanel panelBanner;
    private JPanel panelDashboardContent;
    private IDECardsHolder ideCardsHolder;
    private MenuPanel menuPanel;

    /**
     * Creates new form StartCard
     */
    public StartCard(MenuPanel menuPanel, IDECardsHolder ideCardsHolder) {
        this.menuPanel = menuPanel;
        this.ideCardsHolder = ideCardsHolder;
        init();
        layouts();
    }

    public JPanel getPanelBanner() {
        return panelBanner;
    }

    public void setPanelBanner(JPanel panelBanner) {
        this.panelBanner = panelBanner;
    }

    public JPanel getPanelDashboardContent() {
        return panelDashboardContent;
    }

    public void setPanelDashboardContent(JPanel panelDashboardContent) {
        this.panelDashboardContent = panelDashboardContent;
    }

    public IDECardsHolder getIdeCardsHolder() {
        return ideCardsHolder;
    }

    public void setIdeCardsHolder(IDECardsHolder ideCardsHolder) {
        this.ideCardsHolder = ideCardsHolder;
    }

    private void init() {
        panelBanner = new JPanel();
        panelDashboardContent = new JPanel();
    }

    private void layouts() {
        MigLayout mig = new MigLayout("insets 0", "[]", "[]");
        setLayout(mig);
        menuPanel = new MenuPanel();
        MenuPanelMouseEvent menuPanelMouseEvent = new MenuPanelMouseEvent(ideCardsHolder);
        menuPanel.getLblBasicConfiguraion().addMouseListener(menuPanelMouseEvent);
        
        add(menuPanel, "width 300!,height 100%");
        add(new JSeparator(JSeparator.VERTICAL), "growy");
    }
}
