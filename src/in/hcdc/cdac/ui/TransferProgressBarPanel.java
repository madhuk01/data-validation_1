/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hcdc.cdac.ui;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author piyush
 */
public class TransferProgressBarPanel extends JPanel{
    
    private JLabel lblTotalRecords;
    
    private JLabel lblCompletedRecords;
    
    private JLabel lblTotalRecordsCount;
    
    private JLabel lblCompletedRecordsCount;
    
    private JProgressBar progressbar;

    public TransferProgressBarPanel() {
    }

    public JLabel getLblCompletedRecords() {
        return lblCompletedRecords;
    }

    public void setLblCompletedRecords(JLabel lblCompletedRecords) {
        this.lblCompletedRecords = lblCompletedRecords;
    }

    public JLabel getLblCompletedRecordsCount() {
        return lblCompletedRecordsCount;
    }

    public void setLblCompletedRecordsCount(JLabel lblCompletedRecordsCount) {
        this.lblCompletedRecordsCount = lblCompletedRecordsCount;
    }

    public JLabel getLblTotalRecords() {
        return lblTotalRecords;
    }

    public void setLblTotalRecords(JLabel lblTotalRecords) {
        this.lblTotalRecords = lblTotalRecords;
    }

    public JLabel getLblTotalRecordsCount() {
        return lblTotalRecordsCount;
    }

    public void setLblTotalRecordsCount(JLabel lblTotalRecordsCount) {
        this.lblTotalRecordsCount = lblTotalRecordsCount;
    }

    public JProgressBar getProgressbar() {
        return progressbar;
    }

    public void setProgressbar(JProgressBar progressbar) {
        this.progressbar = progressbar;
    }
}
