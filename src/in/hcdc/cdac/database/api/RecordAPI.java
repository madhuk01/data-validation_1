/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hcdc.cdac.database.api;

import in.hcdc.cdac.detectfiles.CorruptedFileInfo;
import in.hcdc.cdac.util.DBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public class RecordAPI {

    private CorruptedFileInfo info;
    Connection connection;

    public RecordAPI(CorruptedFileInfo info) throws ClassNotFoundException, SQLException {
        this.info = info;
        connection = DBConnection.getConnection();
    }

    public RecordAPI() throws ClassNotFoundException, SQLException {
        connection = DBConnection.getConnection();
    }

    public CorruptedFileInfo getInfo() {
        return info;
    }

    public void setInfo(CorruptedFileInfo info) {
        this.info = info;
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    /**
     *
     * @return @throws SQLException
     * @throws ClassNotFoundException
     */
    public boolean save(CorruptedFileInfo info) throws SQLException, ClassNotFoundException {
        String query = "INSERT INTO RECORD (filename, sourcefilepath, filesize, ismoved, iswellformed, viruscheck, time) VALUES (?,?,?,?,?,?,?)";
        if (connection == null) {
            connection = DBConnection.getConnection();
        }
        int affectedRows = 0;

        PreparedStatement ps;
        int generatedkey = 0;
        ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        try {
            //   ps.setInt(1, info.getId())
            ps.setString(1, info.getFileName());
            ps.setString(2, info.getFilePath());
            ps.setDouble(3, info.getFileSize());
            ps.setBoolean(4, info.isIsMoved());
            ps.setString(5, info.getFileFormatValid());
            ps.setString(6, info.getVirusCheckStatus());
            ps.setString(7, info.getTime());
            affectedRows = ps.executeUpdate();
            // Logger.getInstance().logMessage("Record " + record.getApplicationNO() + " is inserted."); 
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                generatedkey = rs.getInt(1);
                info.setId(generatedkey);
            }

        } catch (SQLException ex) {
            // Logger.getInstance().logMessage(ex);
            ex.printStackTrace();
            return false;
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
        return affectedRows == 1;
    }

    /**
     *
     * @param method to move selected records from table
     *
     */
    public int updateFileTransferStatus(Map<Integer, String> recordsIdlist) throws SQLException {
        int[] result = null;
        String SQL = "UPDATE RECORD SET ismoved=?, transferredfilepath=? WHERE id=?";
        try {
            PreparedStatement stmt = connection.prepareStatement(SQL);
            connection.setAutoCommit(false);
            for (Map.Entry<Integer, String> entry : recordsIdlist.entrySet()) {
                int id = entry.getKey();
                String transferpath = entry.getValue();
                stmt.setBoolean(1, true); // Value for the first parameter, namely 'firstName'
                stmt.setString(2, transferpath);
                stmt.setInt(3, id);
                stmt.addBatch(); // Add to Batch
            }

            result = stmt.executeBatch(); // execute the Batch and commit
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
        return result.length;
    }

    /**
     * method to get details of moved files for report generation
     *
     * @return
     */
    public List<CorruptedFileInfo> getTransferredRecordsDetails() throws SQLException {

        List<CorruptedFileInfo> transferredFilesList = new ArrayList<>();
        Statement stmt = null;
        //  String query = "select COF_NAME, SUP_ID, PRICE, " +  "SALES, TOTAL " + "from " + dbName + ".COFFEES";
        String query = "SELECT Id, filename, sourcefilepath, filesize, ismoved, iswellformed, viruscheck, time,transferredfilepath FROM RECORD WHERE ismoved=1";

        try {
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                CorruptedFileInfo info = new CorruptedFileInfo();
                int id = rs.getInt("id");
                String filename = rs.getString("filename");
                info.setFileName(filename);
                String filepath = rs.getString("sourcefilepath");
                info.setFilePath(filepath);
                String filesize = rs.getString("filesize");
                info.setFileSize(Double.parseDouble(filesize));
                String ismoved = rs.getString("ismoved");
                info.setIsMoved(Boolean.parseBoolean(ismoved));
                String iswellformed = rs.getString("iswellformed");
                info.setFileFormatValid(iswellformed);
                String viruscheck = rs.getString("viruscheck");
                info.setVirusCheckStatus(viruscheck);
                String timestamp = rs.getString("time");
                info.setTime(timestamp);
                String transferredpath = rs.getString("transferredfilepath");
                info.setTransferredfilepath(transferredpath);
                transferredFilesList.add(info);
               // System.out.println("id: "+id+" "+"fn: "+filename+"path: "+filepath+"fiesize: "+filesize+"ismoved: "+ismoved+"iswellfomed: "+iswellformed+"viruscheck: "+viruscheck+"timestamp: "+timestamp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
        return transferredFilesList;
    }

}
