/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hcdc.cdac.detectfiles;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public class CorruptedFileInfo {

    private int Id;
    private String fileName;
    private double fileSize;
    private String filePath;
    private String fileFormatValid;
    private String virusCheckStatus;
    private boolean isMoved;
    private String time;
    private String transferredfilepath;

    public long totalfilesdetected;

    public boolean isIsMoved() {
        return isMoved;
    }

    public void setIsMoved(boolean isMoved) {
        this.isMoved = isMoved;
    }
    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }
    
    public long getTotalfilesdetected() {
        return totalfilesdetected;
    }

    public void setTotalfilesdetected(long totalfilesdetected) {
        this.totalfilesdetected = totalfilesdetected;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public double getFileSize() {
        return fileSize;
    }

    public void setFileSize(double fileSize) {
        this.fileSize = fileSize;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTransferredfilepath() {
        return transferredfilepath;
    }

    public void setTransferredfilepath(String transferredfilepath) {
        this.transferredfilepath = transferredfilepath;
    }
    
    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CorruptedFileInfo other = (CorruptedFileInfo) obj;
        if (!Objects.equals(this.fileName, other.fileName)) {
            return false;
        }
        return true;
    }

    public String getFileFormatValid() {
        return fileFormatValid;
    }

    public void setFileFormatValid(String fileFormatValid) {
        this.fileFormatValid = fileFormatValid;
    }

    public String getVirusCheckStatus() {
        return virusCheckStatus;
    }

    public void setVirusCheckStatus(String virusCheckStatus) {
        this.virusCheckStatus = virusCheckStatus;
    }

   @Override
   public String toString() {
       return "FileName: "+getFileName()+
               "\nPath:"+getFilePath()+
               "\nSize:"+getFileSize()+
               "\nIs valid"+getFileFormatValid()+
               "\nVirus check"+getVirusCheckStatus();
   }
    
    

}
