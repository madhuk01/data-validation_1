/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hcdc.cdac.detectfiles;

import in.hcdc.cdac.util.GetFilesList;
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public class IdentifyCorruptedFiles {
    /**
     * @param path
     * @param executor
     * @param args the command line arguments
     * @return
     */
    private List<Future<ArrayList<CorruptedFileInfo>>> listFiles;
    private Future<ArrayList<CorruptedFileInfo>> submit;
    private int maxThreads;
   
    
    public IdentifyCorruptedFiles(int maxThreads){
        this.maxThreads = maxThreads;
    }
    
    public List<Future<ArrayList<CorruptedFileInfo>>> detectFiles(String path, ExecutorService executor) {
        listFiles = new ArrayList<>();
        File BaseDirectory = new File(path);
        // File archivedDirectory = new File(transit + File.separator + "Archived" + File.separator + srocode);
        File subfond[] = BaseDirectory.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File current, String name) {
                return new File(current, name).isDirectory();
            }
        });
        List<File> fileList = addFilesToList(path);
        System.out.println("Total no of sub directories in Base Directory " + BaseDirectory + "--" + subfond.length + "\n");
        try {
            //   System.out.println("max threads avaliable--" + maxThreads);
            // = Executors.newFixedThreadPool(maxThreads);
            //    System.out.println("Total subfonds size--" + fileList.size());
          //  double fondToDistribute = ((double) subfond.length) / ((double) maxThreads);
            File[] total = fileList.toArray(new File[fileList.size()]);
            double totalSize = total.length;
            double fondToDistribute = ((double) totalSize) / ((double) maxThreads);
            double blocksize = Math.ceil(fondToDistribute);
            System.out.println("block size--" + blocksize);
            System.out.println("-- Total --"+total.length);
            if (blocksize <= 0) {
                blocksize = 1;
            }
            double toId = blocksize;
            double fromId = 1;
            int filesLength = 0;
            for (int i = 1; i <= maxThreads; i++) {
                toId = fromId + blocksize;
                if (toId > fileList.size()) {
                    toId = fileList.size();
                    //     System.out.println("\nStarting : Thread" + i);
                    //     System.out.println("from--" + fromId + "To----" + toId);
                    Callable<ArrayList<CorruptedFileInfo>> worker = new GetFilesList(total, fromId, toId);
                    submit = executor.submit(worker);
                    //System.out.println(":: no of files found ::"+submit.get().size());
                    if (!submit.get().isEmpty()) {
                        listFiles.add(submit);
                    }
                    break;
                } else {
                //   System.out.println("\nStarting : Thread" + i+"\n");
                //   System.out.println("from--" + fromId + "To----" + toId+"\n");
                    Callable<ArrayList<CorruptedFileInfo>> worker = new GetFilesList(total, fromId, toId);
                    submit = executor.submit(worker);
                   // System.out.println("--- submit len ---"+submit.get().size());
                    if(!submit.get().isEmpty()){
                        filesLength = filesLength + submit.get().size();
                    }
                    if (!submit.get().isEmpty()) {
                        listFiles.add(submit);
                    }
                    fromId = toId + 1;
                }
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listFiles;
    }
    /**
     *
     * @param subfond
     * @return
     */
    private ArrayList<File> filesList = new ArrayList<>();
    public ArrayList<File> addFilesToList(String Path) {
          File directory = new File(Path);
          File[] fList = directory.listFiles();
          for(File file: fList){
           if(file.isFile()){
               filesList.add(file);
           }
           else if(file.isDirectory()){
               addFilesToList(file.getAbsolutePath());
           }
       }
       return filesList;
    }
}
