package in.hcdc.cdac.util;

import com.lowagie.text.pdf.PdfReader;
import in.hcdc.cdac.constants.Constants;
import in.hcdc.cdac.detectfiles.CorruptedFileInfo;
import in.hcdc.cdac.openofficefilesvalidate.FormatValidator;
import in.hcdc.cdac.openofficefilesvalidate.OOFCMain;
import in.hdc.cdac.in.antiviruscheck.ClamAVFileScan;
import in.hdc.cdac.in.fileformatvalidation.JhoveInit;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.Callable;

/**
 *
 * @author <madhusudhankk@cdac.in>
 *
 * @version 1
 * @since 1
 * @see in.hcdc.cdac.IndentifyCorruptedFiles
 */
public class GetFilesList implements Callable<ArrayList<CorruptedFileInfo>> {

    private ArrayList<CorruptedFileInfo> filesList = null;
    private File[] totalRecords;
    private double from;
    private double to;

    public GetFilesList(File[] totalRecords, double from, double to) {
        this.totalRecords = totalRecords;
        this.from = from;
        this.to = to;
        filesList = new ArrayList<>();
    }

    @Override
    public ArrayList<CorruptedFileInfo> call() throws Exception {
        // System.out.println("--total files --"+totalRecords.length);
        Boolean flag = null;
        String errorMessage = null;
        System.out.println("from: " + from + " to: " + to);
        for (double i = from; i <= to; i++) {
            int t = (int) i - 1;
            File file3 = totalRecords[t];
            // System.out.println("--t--"+t+" exists: "+file3.exists());
            if (file3.isFile()) {
                //          System.out.println("from: "+i+" to: "+to);
                CorruptedFileInfo info = new CorruptedFileInfo();
                String filename = file3.getName();
                double fileSizeInMB = Helper.convertFilebyteToMB(file3);
                info.setFileName(file3.getName());
                info.setFilePath(file3.getAbsolutePath());
                info.setFileSize(fileSizeInMB);
                /**
                 * *
                 * Check for virus on this file
                 */
                ClamAVFileScan clamAVFileScan = new ClamAVFileScan(file3);
                boolean isInfected = clamAVFileScan.scanResult();
                if (isInfected) {
                    info.setVirusCheckStatus("No virus found");
                } else {
                    info.setVirusCheckStatus("Infected!!");
                }
                if (file3.length() != 0) {
                    String[] split = filename.split("\\.");
                    String fileExtention = split[split.length - 1];
                    switch (fileExtention.toUpperCase()) {
                        /**
                         * *
                         * Check whether file is well-formed and valid using
                         * Jhove
                         */
                        case Constants.FORMAT_JPG:
                        case Constants.FORMAT_JPEG:
                        case Constants.FORMAT_XML:
                        case Constants.FORMAT_TXT:
                            flag = checkJhoveFile(file3);
                            break;
                        case Constants.FORMAT_PDF:
                            PdfReader reader = null;
                            if (file3.exists()) {
                                try {
                                    reader = new PdfReader(file3.getAbsolutePath());
                                    flag = true;
                                    // info.setFileFormatValid("valid");
                                } catch (Exception e) {
                                    // info.setFileFormatValid("invalid");
                                    flag = false;
                                    if (reader != null) {
                                        reader.close();
                                    }
                                }
                            }
                            break;
                        case Constants.FORMAT_ODP:
                        case Constants.FORMAT_ODS:
                        case Constants.FORMAT_ODF:
                        case Constants.FORMAT_ODT:
                            OOFCMain oofcvalidation = new OOFCMain();
                            flag = oofcvalidation.odfvalidator(file3);
                            break;
                        case Constants.FORMAT_OOXML:
                            flag = FormatValidator.getOOXMLValidator().validate(file3);
                            break;
                        /*      case Constants.FORMAT_DOC:
                            case Constants.FORMAT_PPT:
                            case Constants.FORMAT_PPTX:
                            case Constants.FORMAT_XLSX:
                            case Constants.FORMAT_DOCX:
                                flag = true;
                                break;
                            case Constants.FORMAT_ODP:
                            case Constants.FORMAT_ODT:
                            case Constants.FORMAT_ODS:
                                flag = true;
                                break;  */
                        default:
                            errorMessage = "Well-formed";
                    }

                } else {
                    errorMessage = "Blank file";
                }
                if (flag != null) {
                    errorMessage = (flag == false) ? "Invalid file format" : "Well-formed";
                }
                info.setFileFormatValid(errorMessage);
                Date d = new Date();
                info.setTime(d.toString());
                filesList.add(info);
            } else if (file3.isDirectory()) {
                System.out.println(file3.getName() + "--dir--");
            }
        }
        //  System.out.println("--added size--"+filesList.size());
        return filesList;
    }

    public boolean checkJhoveFile(File file) {
        JhoveInit ji = new JhoveInit();
        boolean isvalid = ji.init(file);
        return isvalid;
    }

}
