/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hcdc.cdac.util;

import java.io.File;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public class Helper {
    
    private static final long  MEGABYTE = 1024L * 1024L;
    
        public static String getFormattedPageTitle(String title) {
        String html = "<HTML>";
        html += "<font size=6 color=#006699>" + title + "</font>";
        html += "</HTML>";
        return html;
    }

 /*   public static double convertFilebyteToMB(File file) {
        long fileSizeInBytes = file.length();
// Convert the bytes to Kilobytes (1 KB = 1024 Bytes)
        long fileSizeInKB = fileSizeInBytes / 1024;
// Convert the KB to MegaBytes (1 MB = 1024 KBytes)
        double fileSizeInMB = fileSizeInKB / 1024;
        return fileSizeInMB;
    } */
    
    public static double convertFilebyteToMB(File file) {
         double fileSizeInBytes = file.length();
         double fileSizeInMB = fileSizeInBytes / MEGABYTE;
         
           fileSizeInMB = fileSizeInMB*1000;
           fileSizeInMB = (double)((int) fileSizeInMB);
           fileSizeInMB = fileSizeInMB /1000;
         
         return fileSizeInMB;
    }   
}
