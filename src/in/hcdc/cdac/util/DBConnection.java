/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hcdc.cdac.util;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import static in.hcdc.cdac.constants.Constants.*;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public class DBConnection {

    private static Connection connection = null;
    private static final String MYSQL_JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static String host;
    private static String port;
    private static String dbSchemaName;
    private static String dbLoginName;
    private static String dbLoginPwd;
    private static String outputPath;

    public static String getOutputPath() {
        return outputPath;
    }

    public static void setOutputPath(String outputPath) {
        DBConnection.outputPath = outputPath;
    }

    public static String getDbLoginName() {
        return dbLoginName;
    }

    public static void setDbLoginName(String dbLoginName) {
        DBConnection.dbLoginName = dbLoginName;
    }

    public static String getDbLoginPwd() {
        return dbLoginPwd;
    }

    public static void setDbLoginPwd(String dbLoginPwd) {
        DBConnection.dbLoginPwd = dbLoginPwd;
    }

    public static String getDbSchemaName() {
        return dbSchemaName;
    }

    public static void setDbSchemaName(String dbSchemaName) {
        DBConnection.dbSchemaName = dbSchemaName;
    }

    public static String getHost() {
        return host;
    }

    public static void setHost(String host) {
        DBConnection.host = host;
    }

    public static String getPort() {
        return port;
    }

    public static void setPort(String port) {
        DBConnection.port = port;
    }

    public static Connection getConnection() throws ClassNotFoundException, SQLException {
        try {
            Class.forName(MYSQL_JDBC_DRIVER);
            String Url = "jdbc:mysql://" + getHost() + ":" + getPort() + "/" + getDbSchemaName() + getDbLoginName() + getDbLoginPwd();
//            System.out.println("url "+Url);
            connection = DriverManager.getConnection("jdbc:mysql://" + getHost() + ":" + getPort() + "/" + getDbSchemaName(), getDbLoginName(), getDbLoginPwd());
            // Logger.getInstance().logMessage("Connection established.");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Unable to connect database", "DATABASE CONNECTION", JOptionPane.ERROR_MESSAGE);
            // Logger.getInstance().logMessage("SQLException in setConnection method of DBConnection.");
            // Logger.getInstance().logMessage(ex);
        } catch (ClassNotFoundException ex) {
//            ex.printStackTrace();
            // Logger.getInstance().logMessage("ClassNotFoundException in setConnection method of DBConnection.");
            // Logger.getInstance().logMessage(ex);
        }
        return connection;
    }

    public static void setConnection(Connection connection) {
        DBConnection.connection = connection;
    }

    public static boolean getSettingFile() {
        boolean flag = true;
        String path = System.getProperty(USER_HOME);
        String finalPath = path + File.separator + DATAVALIDATION_DIRECTORY;

        File f = new File(finalPath);
        if (!f.exists()) {
            f.mkdir();
        }

        FileInputStream file = null;
        try {
            File dbfile = new File(finalPath + File.separator + DB_CONFIG);
            if (dbfile.exists()) {
                file = new FileInputStream(dbfile);
                DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = builderFactory.newDocumentBuilder();
                Document xmlDocument = builder.parse(file);

                XPath xPath = XPathFactory.newInstance().newXPath();

                String host = xPath.compile("/dbconfigure/host").evaluate(xmlDocument);
                String port = xPath.compile("/dbconfigure/port").evaluate(xmlDocument);
                String database = xPath.compile("/dbconfigure/database").evaluate(xmlDocument);
                String username = xPath.compile("/dbconfigure/username").evaluate(xmlDocument);
                String password = xPath.compile("/dbconfigure/password").evaluate(xmlDocument);
                // Logger.getInstance().logMessage("The database details are loaded.");
                DBConnection.setHost(host);
                DBConnection.setPort(port);
                DBConnection.setDbSchemaName(database);
                DBConnection.setDbLoginName(username);
                DBConnection.setDbLoginPwd(password);
            } else {
                JOptionPane.showMessageDialog(null, "DataBase Configuration file is not exists", "DATABASE Configuration Error", JOptionPane.ERROR_MESSAGE);
            }

        } catch (Exception ex) {
            flag = false;
            // Logger.getInstance().logMessage(ex);
            // Logger.getInstance().logMessage("Exception in getSettingFile method of DBConnection");
        }
        return flag;
    }

}
