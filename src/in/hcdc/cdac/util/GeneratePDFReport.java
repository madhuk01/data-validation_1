/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hcdc.cdac.util;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import in.hcdc.cdac.database.api.RecordAPI;
import in.hcdc.cdac.detectfiles.CorruptedFileInfo;
import in.hcdc.cdac.main.IDE;
import in.hcdc.cdac.ui.TransferProgressBarPanel;
import java.awt.Color;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public class GeneratePDFReport extends Task {
    private final File sourceDirectory;
    private final File targetDirectory;
    private final long numberOfFoldersToPick;
    private TransferProgressBarPanel progressbarPanel;
    private List<CorruptedFileInfo> targetPathFilesList;
    
    
    public GeneratePDFReport(File sourceDirectory, File targetDirectory, long numberOfFoldersToPick) {
        this.sourceDirectory = sourceDirectory;
        this.targetDirectory = targetDirectory;
        this.numberOfFoldersToPick = numberOfFoldersToPick;
        if (!(this.sourceDirectory.isDirectory() && this.targetDirectory.isDirectory())) {
            throw new IllegalArgumentException("Source and target must be directories");
        }
    }
    
    /**
     * Method to transfer files from one directory to another.
     *
     * @throws IOException
     */
    public void transfer() throws SQLException, ClassNotFoundException {
        RecordAPI recordAPI = new RecordAPI();
        if (this.sourceDirectory.isDirectory() && this.targetDirectory.isDirectory()) {
            DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
            OutputStream pdfFile = null;
            File[] file = null;
            Document document = new Document();
            try {
                /**
                 * Creating images for transfer status.
                 */
                Image successImage = null;
                Image failureImage = null;
                Image datavalidationlogo = null;
                try {
                    successImage = Image.getInstance(IDE.class.getResource("/in/hcdc/cdac/resources/images/checkmark.jpg"));
                    successImage.scaleAbsolute(10f, 10f);
                    failureImage = Image.getInstance(IDE.class.getResource("/in/hcdc/cdac/resources/images/error.png"));
                    failureImage.scaleAbsolute(10f, 10f);
                    datavalidationlogo = Image.getInstance(IDE.class.getResource("/in/hcdc/cdac/resources/images/main_logo.png"));
                } catch (Exception e) {
                    e.printStackTrace();

                }
                /**
                 * Create log pdf file.
                 */
                String logsPath = System.getProperty("user.home") + File.separator + ".data-validation" + File.separator + "transfer-logs" + File.separator;

                Calendar currentDate = Calendar.getInstance();
                SimpleDateFormat formatter = new SimpleDateFormat("MMM_dd_yyyy");
                String dateNow = formatter.format(currentDate.getTime());
                DateTime dateTime = new DateTime();
                File logFile = new File(logsPath, sourceDirectory.getParentFile().getName() + "_" + sourceDirectory.getName() + "_" + StringUtils.replaceChars(fmt.print(dateTime), ":", ".") + ".pdf");
                pdfFile = new FileOutputStream(logFile);
                PdfWriter.getInstance(document, pdfFile);
                targetPathFilesList = recordAPI.getTransferredRecordsDetails();
                System.out.println(targetPathFilesList);
                /**
                 * Table object for log.
                 */
                PdfPTable logTable = new PdfPTable(6);
                float[] columnWidths = new float[]{10f, 28f, 22f, 30f, 22f, 20f};
                logTable.setWidths(columnWidths);
                logTable.setWidthPercentage(100);
                /**
                 * Creating header cell.
                 */
                PdfPCell headerCell = new PdfPCell(new Paragraph("Data-validation File Transfer status"));
                headerCell.setColspan(6);
                headerCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                headerCell.setPadding(10.0f);
                headerCell.setBackgroundColor(Color.decode("#ccddee"));
                logTable.addCell(headerCell);
                /**
                 * Creating sub headers cell.
                 */
                logTable.addCell("Sr. No");
                logTable.addCell("File Name");
                logTable.addCell("Date/Time");
                logTable.addCell("Source Path");
                logTable.addCell("Destination Path");
                logTable.addCell("Moved Status");
         //       file = sourceDirectory.listFiles(_TEMP_FILE_FILTER);
         //       long looplimit = file.length > numberOfFoldersToPick ? numberOfFoldersToPick : file.length;
//                  long looplimit = targetPathFilesList.size();
//                if (progressbarPanel != null) {
//                    progressbarPanel.getLblTotalRecordsCount().setText(String.valueOf(looplimit));
//                    progressbarPanel.setVisible(true);
//                }
            /*    for (int i = 0; i < looplimit; i++) {
                    int recordCount = i + 1;
                    PdfPCell imageCell = null;
                    PdfPCell sourceSizeCell = null;
                    PdfPCell targetSizecell = null;
                    PdfPCell dateTimeCell = null;
                    
                    try {
                        double sizeAtSource = (double) FileUtils.sizeOfDirectory(file[i]) / 1024 / 1024;
                        String unit = "MB";
//                        if (sizeAtSource < 1) {
//                            sizeAtSource = (double) sizeAtSource / 1024;
//                            unit = "KB";
//                        }
                        FileUtils.moveDirectoryToDirectory(file[i], this.targetDirectory, false);
                        if (progressbarPanel != null) {
                            long completedRecords = i + 1;
                            Long percentComepleted = (completedRecords * 100) / looplimit;
                            progressbarPanel.getLblCompletedRecordsCount().setText(String.valueOf(completedRecords));
                            progressbarPanel.getProgressbar().setValue(percentComepleted.intValue());
                            progressbarPanel.getProgressbar().setStringPainted(true);
                        }
                        /**
                         * Add logs to table and then store it in pdf file.
                         
                        double sizeAtTarget = (double) FileUtils.sizeOfDirectory(new File(targetDirectory, file[i].getName())) / 1024 / 1024;

                        logTable.addCell("" + recordCount);
                        logTable.addCell(file[i].getName());
                        dateTime = new DateTime();
                        logTable.addCell(fmt.print(dateTime));
                        sourceSizeCell = new PdfPCell(new Phrase(String.format("%.3f", sizeAtSource) + " " + unit));
                        sourceSizeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        logTable.addCell(sourceSizeCell);
                        targetSizecell = new PdfPCell(new Phrase(String.format("%.3f", sizeAtTarget) + " " + unit));
                        targetSizecell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        logTable.addCell(targetSizecell);
                        imageCell = new PdfPCell(successImage);
                        imageCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                        imageCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        logTable.addCell(imageCell);

                    } catch (IOException ex) {
                        logTable.addCell("" + recordCount);
                        logTable.addCell(file[i].getName());
                        dateTime = new DateTime();
                        logTable.addCell(fmt.print(dateTime));
                        logTable.addCell("-");
                        logTable.addCell("-");
                        imageCell = new PdfPCell(failureImage);
                        imageCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                        imageCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        logTable.addCell(imageCell);
                        //log this exception in report
                    }
                } */
                logTable.setSpacingBefore(30.0f);
                logTable.setSpacingAfter(30.0f);

                document.open();
                document.addTitle("Data-validation File Transfer status");
                document.addAuthor("HCDC");
                document.addCreationDate();

                document.add(datavalidationlogo);
           //     document.add(new Paragraph("Transfer Done By :" + this.userName));
                dateTime = new DateTime();
                document.add(new Paragraph("Report Generated On :" + fmt.print(dateTime)));
                document.add((new Paragraph("Source Directory   :" + this.sourceDirectory.getAbsolutePath())));
                document.add((new Paragraph("Target Directory   :" + this.targetDirectory.getAbsolutePath())));
                document.add(logTable);

            } catch (DocumentException ex) {
                try {
                    ex.printStackTrace();
                    throw new DocumentException("Unable to generate Logs" + ex.getMessage());
                } catch (DocumentException ex1) {
                    Logger.getLogger(GeneratePDFReport.class.getName()).log(Level.SEVERE, null, ex1);
                }
            } catch (FileNotFoundException ex) {
                Logger.getLogger(GeneratePDFReport.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    if (document != null) {
                        //document.close();
                    }
                    if (pdfFile != null) {
                        pdfFile.close();
                    }
                } catch (IOException ex) {
                }

            }
        } else {
            throw new IllegalArgumentException("Source and Target Folders must be a directory");
        }
        
        if(progressbarPanel!=null)
            progressbarPanel.setVisible(false);
      
    }

    @Override
    public void run() {
        try {

            System.out.println("task running on " + new Date());
            transfer();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    public final static FileFilter _TEMP_FILE_FILTER = new FileFilter() {

        @Override
        public boolean accept(File dir) {
            return dir.isDirectory() && !dir.getName().equalsIgnoreCase("temp");
        }
    };
}
