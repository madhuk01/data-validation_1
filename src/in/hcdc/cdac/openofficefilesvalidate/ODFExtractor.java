/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hcdc.cdac.openofficefilesvalidate;

import java.io.File;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public class ODFExtractor
  implements IExtractor
{
  @Override
  public File unzipFile(String sourceFile, String unzipLocation) throws ZipException
  {
    String destination = unzipLocation + sourceFile.substring(sourceFile.lastIndexOf(File.separator) + 1, sourceFile.lastIndexOf("."));
    try
    {
      ZipFile zipFile = new ZipFile(sourceFile);
      zipFile.extractAll(destination);
      return new File(destination);
    }
    catch (ZipException e)
    {
      throw e;
    }
  }
}
