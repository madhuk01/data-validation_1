/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hcdc.cdac.openofficefilesvalidate.util;

import java.io.File;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public class ODFSchemaLoader
{
  public static File loadManifestSchema(String version, String schemaLocation)
  {
    File[] files = load(ODFSchemaType.MANIFEST, version, schemaLocation);
    if ((files != null) && (files.length > 0)) {
      return files[0];
    }
    return null;
  }

  public static File loadODFSchema(String version, String schemaLocation)
  {
    File[] files = load(ODFSchemaType.ODF, version, schemaLocation);
    if ((files != null) && (files.length > 0)) {
      return files[0];
    }
    return null;
  }

  private static File[] load(ODFSchemaType schematype, String version, String schemaLocation) {
    return new File(schemaLocation).listFiles(new ODFSchemaFileFilter(version, schematype));
  }
}