/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hcdc.cdac.openofficefilesvalidate.util;

import java.io.File;
import java.io.FileFilter;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public class ODFSchemaFileFilter implements FileFilter {

    private String version;
    private ODFSchemaType type;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public ODFSchemaType getType() {
        return type;
    }

    public void setType(ODFSchemaType type) {
        this.type = type;
    }

    public ODFSchemaFileFilter(String version, ODFSchemaType type) {
        this.version = version;
        this.type = type;
    }

    @Override
    public boolean accept(File pathname) {
        if (pathname.getName().contains(this.version + "-" + this.type.getType())) {
            return true;
        }
        return false;
    }

}
