/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hcdc.cdac.openofficefilesvalidate.util;

import java.io.File;
import java.io.FileFilter;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public class ContentXmlFileFilter
  implements FileFilter
{
  public boolean accept(File contextXml)
  {
    if ("content.xml".equalsIgnoreCase(contextXml.getName())) {
      return true;
    }
    return false;
  }
}
