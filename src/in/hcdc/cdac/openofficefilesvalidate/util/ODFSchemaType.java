/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hcdc.cdac.openofficefilesvalidate.util;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public enum ODFSchemaType
{
  MANIFEST("os-manifest-schema"), 
  ODF("os-schema");

  private String type;

  private ODFSchemaType(String type) { this.type = type; }

  public String getType()
  {
    return this.type;
  }
}
