/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hcdc.cdac.openofficefilesvalidate;

import com.thaiopensource.util.PropertyMapBuilder;
import com.thaiopensource.validate.ValidateProperty;
import com.thaiopensource.validate.ValidationDriver;
import com.thaiopensource.xml.sax.ErrorHandlerImpl;

import in.hcdc.cdac.openofficefilesvalidate.util.ContentXmlFileFilter;
import in.hcdc.cdac.openofficefilesvalidate.util.MetaXmlFileFilter;
import in.hcdc.cdac.openofficefilesvalidate.util.ODFSchemaLoader;
import in.hcdc.cdac.openofficefilesvalidate.util.ODFSchemaType;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import net.lingala.zip4j.exception.ZipException;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public class ODFValidator implements IValidator {

    private static String odfVersion;
    private final String schemaLocation;
    private final String unziplocation;
    private final ODFExtractor extractor;
    private final ODFVersionChecker versionChecker;

    public ODFValidator(String schemaLocation, String unzipLocation) {
        this.schemaLocation = schemaLocation;
        this.unziplocation = unzipLocation;
        this.extractor = new ODFExtractor();
        this.versionChecker = new ODFVersionChecker();
    }

    @Override
    public boolean validate(File sourceFile) {
        boolean isValid = false;
        File extractedDirectory = null;
        try {
            extractedDirectory = this.extractor.unzipFile(sourceFile.getAbsolutePath(), unziplocation);

            File[] files = extractedDirectory.listFiles(new ContentXmlFileFilter());
            File contentXmlFile = null;
            File metaXmlFile = null;
            if (files.length > 0) {
                contentXmlFile = files[0];
            }
            File mainfestXMLFile = new File(extractedDirectory, "META-INF" + File.separator + "manifest.xml");

            odfVersion = this.versionChecker.checkVersion(contentXmlFile);
            if (odfVersion == null) {
                files = extractedDirectory.listFiles(new MetaXmlFileFilter());
                if (files.length > 0) {
                    metaXmlFile = files[0];
                }
                odfVersion = this.versionChecker.checkVersionFromMeta(metaXmlFile);
            }
            if (odfVersion == null) {
                odfVersion = "1.2";
            }
            File mainFestSchemaFile = getODFSchemaFile(odfVersion, ODFSchemaType.MANIFEST, this.schemaLocation);
            File odfSchemaFile = getODFSchemaFile(odfVersion, ODFSchemaType.ODF, this.schemaLocation);

            ErrorHandler seh = new ErrorHandlerImpl();
            PropertyMapBuilder builder = new PropertyMapBuilder();
            builder.put(ValidateProperty.ERROR_HANDLER, seh);
            ValidationDriver validator = new ValidationDriver(builder.toPropertyMap());

            InputSource contentXmlSource = ValidationDriver.fileInputSource(contentXmlFile);
            InputSource mainfestXMLSource = ValidationDriver.fileInputSource(mainfestXMLFile);
            InputSource manifestSchemaSource = ValidationDriver.fileInputSource(mainFestSchemaFile);
            InputSource odfSchemaSource = ValidationDriver.fileInputSource(odfSchemaFile);
            validator.loadSchema(manifestSchemaSource);
            isValid = validator.validate(mainfestXMLSource);
            if (isValid) {
                validator.loadSchema(odfSchemaSource);
                for (File file : extractedDirectory.listFiles()) {
                    String mimeType = Files.probeContentType(file.toPath());
                    if ("application/xml".equalsIgnoreCase(mimeType)) {
                        isValid = validator.validate(contentXmlSource);
                    }
                }
            }
        } catch (IOException | SAXException | ZipException ex) {
            ex.printStackTrace();
        }  finally {
            if ((extractedDirectory != null) && (extractedDirectory.exists())) {
                delete(extractedDirectory);
            }
        }
        return isValid;
    }

    public static void delete(File file) {
        if (file.isDirectory()) {
            if (file.list().length == 0) {
                file.delete();
            } else {
                String[] files = file.list();
                for (String temp : files) {
                    File fileDelete = new File(file, temp);

                    delete(fileDelete);
                }

                if (file.list().length == 0) {
                    file.delete();
                }
            }
        } else {
            file.delete();
        }
    }

    private File getODFSchemaFile(String odfVersion, ODFSchemaType type, String schemaLocation) {
        //  switch (1.$SwitchMap$com$hcdc$coedp$format$validator$odf$util$ODFSchemaType[type.ordinal()]) {
        switch (type.ordinal()) {
            case 0:
                return ODFSchemaLoader.loadManifestSchema(odfVersion, schemaLocation);
            case 1:
                return ODFSchemaLoader.loadODFSchema(odfVersion, schemaLocation);
        }
        return null;
    }

}
