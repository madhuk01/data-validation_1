/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hcdc.cdac.openofficefilesvalidate;

import java.io.File;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public abstract interface IValidator {
    public abstract boolean validate(File paramFile);
}
