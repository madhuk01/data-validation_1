/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hcdc.cdac.openofficefilesvalidate;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public class FormatValidator {

    private static IValidator odfValidator;
    private static IValidator ooXMLValidator;

    public static IValidator getODFValidator(String schemaLocation, String unzipLocation) {

        if (odfValidator == null) {
            odfValidator = new ODFValidator(schemaLocation, unzipLocation);
        }
        return odfValidator;
    }

    public static IValidator getOOXMLValidator() {
        if (ooXMLValidator == null) {
             ooXMLValidator = new OOXMLValidator();
               }
        return ooXMLValidator;
    }

}
