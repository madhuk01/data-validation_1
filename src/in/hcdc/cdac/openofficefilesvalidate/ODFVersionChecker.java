/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hcdc.cdac.openofficefilesvalidate;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public class ODFVersionChecker implements IVersionChecker {

    @Override
    public String checkVersion(File contentFile) {
        DocumentBuilder documentBuilder = null;
        DocumentBuilderFactory factory = null;
        String version = null;
        try {
            factory = DocumentBuilderFactory.newInstance();
            factory.setNamespaceAware(true);
            documentBuilder = factory.newDocumentBuilder();
            Document doc = documentBuilder.parse(contentFile);
            XPath xPath = XPathFactory.newInstance().newXPath();

            String expression = "//*[local-name()='document-content']";
            Node node = (Node) xPath.compile(expression).evaluate(doc, XPathConstants.NODE);
            version = node.getAttributes().getNamedItem("office:version").getNodeValue();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return version;
    }

    public String checkVersionFromMeta(File metaFile) {
        DocumentBuilder documentBuilder = null;
        DocumentBuilderFactory factory = null;
        String version = null;
        try {
            factory = DocumentBuilderFactory.newInstance();
            factory.setNamespaceAware(true);
            documentBuilder = factory.newDocumentBuilder();
            Document doc = (Document) documentBuilder.parse(metaFile);
            XPath xPath = XPathFactory.newInstance().newXPath();

            String expression = "//*[local-name()='document-meta']";
            Node node = (Node) xPath.compile(expression).evaluate(doc, XPathConstants.NODE);
            version = node.getAttributes().getNamedItem("office:version").getNodeValue();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return version;
    }

}
