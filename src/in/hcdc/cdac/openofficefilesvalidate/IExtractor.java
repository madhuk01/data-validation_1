/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hcdc.cdac.openofficefilesvalidate;

import java.io.File;
import net.lingala.zip4j.exception.ZipException;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public interface IExtractor {

    public abstract File unzipFile(String paramString1, String paramString2)
            throws ZipException;
}
