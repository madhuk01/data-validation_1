/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hcdc.cdac.openofficefilesvalidate;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.UUID;
import org.probatron.officeotron.ValidationReport;
import org.probatron.officeotron.OOXMLValidationSession;
import org.probatron.officeotron.ReportFactory;
import org.probatron.officeotron.sessionstorage.Store;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
class OOXMLValidator implements IValidator {

    public OOXMLValidator() {
    }

    @Override
    public boolean validate(File sourceFile) {
        OOXMLValidationSession ovs = null;
        UUID uuid = null;
        Store.init(System.getProperty("java.io.tmpdir"), false);
        try {
            uuid = Store.putZippedResource(new FileInputStream(sourceFile), sourceFile.getPath());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        ovs = new OOXMLValidationSession(uuid, new ReportFactory() {
            public ValidationReport create() {
                // return new OOXMLValidator.HelperValidationReport(OOXMLValidator.this, null);
                return new OOXMLValidator.HelperValidationReport();
            }
        });

        if (ovs != null) {
            ovs.validate();
            if (ovs.getErrCount() == 1) {
                return true;
            }
            return false;
        }
        return false;
    }

    private class HelperValidationReport implements ValidationReport {

        private HelperValidationReport() {
        }

        public void addComment(String s) {
        }

        public void addComment(String klass, String s) {
        }

        public void decIndent() {
        }

        public void endReport() {
        }

        public int getErrCount() {
            return 0;
        }

        public void incErrs() {
        }

        public void incIndent() {
        }

        public void streamOut()
                throws IOException {
        }
    }

}
