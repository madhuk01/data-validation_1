/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hcdc.cdac.openofficefilesvalidate;

import java.io.File;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public class OOFCMain {

    public boolean odfvalidator(File inputFile) {
        boolean validformat = false;
        try {
            String unzipLocation = "C:\\Users\\admin\\.org\\appdata";

            String schemaLocation = "C:\\Users\\admin\\.org\\schema";

            IValidator ivalidator = FormatValidator.getODFValidator(schemaLocation, unzipLocation);

            if (ivalidator.validate(inputFile)) {
                validformat = true;
            } else {
                validformat = false;
            }

        } catch (Exception ex) {

            ex.printStackTrace();
        }

        return validformat;
    }

}
