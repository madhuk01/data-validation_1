/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hcdc.cdac.logger;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public class LoggerConstants {

    private static FileLogger newLogger;
    private static LoggerConstants instance = new LoggerConstants();

    public static LoggerConstants getInstance() {
        return instance;
    }

    public FileLogger getLogger() {
        return newLogger;
    }

    public void addLogger(FileLogger newLogger) {
        this.newLogger = newLogger;
    }
}
