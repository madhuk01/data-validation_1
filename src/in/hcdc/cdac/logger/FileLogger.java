/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hcdc.cdac.logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public class FileLogger {

    private OutputStream fos;
    private OutputStreamWriter osw;
    private PrintWriter logWriter;
    private File logFile;

    public FileLogger(String logFileName) throws LoggingException {

        logFile = new File(logFileName);

        if (logFile.exists() && !logFile.canWrite()) {
            throw new LoggingException("Cannot write to log file: " + logFile);
        }

        try {
            fos = new FileOutputStream(logFileName, true);
            osw = new OutputStreamWriter(fos);
            osw.write("------------------------------------------- New Application Started -------------------------------------------");
            logWriter = new PrintWriter(osw);
            logWriter.println();
        } catch (IOException e) {
            throw new LoggingException("Cannot open " + logFileName);
        }
    }

    public void log(String message) {
        writeToLog(message);
        logWriter.flush();
    }

    public void log(Throwable t) {
        writeToLog(t);
        logWriter.flush();
    }

    protected void writeToLog(String str) {
        try {
            logWriter.println(str);
            logWriter.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void writeToLog(Throwable t) {
        StringWriter stringWriter = new StringWriter();
        t.printStackTrace(new PrintWriter(stringWriter));
        writeToLog(stringWriter.toString());
    }
}
