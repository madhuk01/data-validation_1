/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hcdc.cdac.logger;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public class Logger {

    public static synchronized Logger getInstance() {
        if (self == null) {
            self = new Logger();
        }
        return self;
    }

    private Logger() {
        logger = LoggerConstants.getInstance().getLogger();
    }

    public void logMessage(String msg) {
        logger.log(msg);
    }

    public void logMessage(Throwable e) {
        logger.log(e);
    }
    private static Logger self = null;
    private FileLogger logger;
}
