/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hcdc.cdac.main;

import static in.hcdc.cdac.constants.Constants.CARD_DASHBOARD;
import static in.hcdc.cdac.constants.Constants.MENU_ITEM_BASIC_CONFIGURATION;
import in.hcdc.cdac.logger.Logger;
import in.hcdc.cdac.ui.MenuPanel;
import in.hcdc.cdac.ui.OutputPathPanel;
import in.hcdc.cdac.ui.StartCard;
import in.hcdc.cdac.ui.event.OutputPathPanelEvent;
import java.awt.CardLayout;
import javax.swing.JPanel;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public class IDECardsHolder extends JPanel {
    
    private CardLayout cLayout;
    private StartCard startCard;
    private MenuPanel menuPanel;
    private OutputPathPanel outputPathPanel;
    private OutputPathPanelEvent outputPathPanelEvent;
    private String directoryPath;   

    public CardLayout getcLayout() {
        return cLayout;
    }

    public void setcLayout(CardLayout cLayout) {
        this.cLayout = cLayout;
    }

    public StartCard getStartCard() {
        return startCard;
    }

    public void setStartCard(StartCard startCard) {
        this.startCard = startCard;
    }

    public MenuPanel getMenuPanel() {
        return menuPanel;
    }

    public void setMenuPanel(MenuPanel menuPanel) {
        this.menuPanel = menuPanel;
    }

    public OutputPathPanel getOutputPathPanel() {
        return outputPathPanel;
    }

    public void setOutputPathPanel(OutputPathPanel outputPathPanel) {
        this.outputPathPanel = outputPathPanel;
    }

    public String getDirectoryPath() {
        return directoryPath;
    }

    public void setDirectoryPath(String directoryPath) {
        this.directoryPath = directoryPath;
    }

    public IDECardsHolder() {
        try {
            cLayout = new CardLayout();
            setLayout(cLayout);
            initializeCards();
            addCards();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addCards() {
        add(startCard, CARD_DASHBOARD);
        add(outputPathPanel, MENU_ITEM_BASIC_CONFIGURATION);
    }

    public void initializeCards() {
        try {
            menuPanel = new MenuPanel();
            // System.out.println(":: Menu panel ::"+menuPanel);
            startCard = new StartCard(menuPanel, this);
           // System.out.println(":: Start Card ::"+startCard);
            outputPathPanel = new OutputPathPanel(menuPanel, this);
            outputPathPanelEvent = new OutputPathPanelEvent(outputPathPanel, this);
            System.out.println("inside initialize cards");
            outputPathPanel.getBtnSave().addActionListener(outputPathPanelEvent);
            outputPathPanel.getBtnReset().addActionListener(outputPathPanelEvent);
            outputPathPanel.getBtntranseferReset().addActionListener(outputPathPanelEvent);
            outputPathPanel.getBtnCancel().addActionListener(outputPathPanelEvent); 
            outputPathPanel.getBtnSubmit().addActionListener(outputPathPanelEvent);
            outputPathPanel.getBtnMove().addActionListener(outputPathPanelEvent);
            outputPathPanel.getBtnReportDnd().addActionListener(outputPathPanelEvent);
           // filesListPanel = new FilesListPanel(this);

        } catch (Exception e) {
            e.printStackTrace();
            Logger.getInstance().logMessage(e);
        }
        return;
    }
}
