/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hcdc.cdac.main;

import static in.hcdc.cdac.constants.Constants.*;
import in.hcdc.cdac.logger.Logger;
import in.hcdc.cdac.ui.IDEMenubar;
import in.hcdc.cdac.ui.event.IDEEvent;
import in.hcdc.cdac.util.DBConnection;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public class IDE {
    
    private static IDEMenubar menubar;
    private static IDECardsHolder cardsHolder;
    public static JFrame frame;
    private static File configurationDirectoy,transferlogDirectory;

    public static void main(String[] args) {
        try{
            configurationDirectoy = new File(System.getProperty("user.home"), DATAVALIDATION_DIRECTORY);
            if (!configurationDirectoy.exists()) {
                if (!configurationDirectoy.mkdir()) {
                    JOptionPane.showMessageDialog(null, "Can't create configuration directory");
                };
            }
            
//            transferlogDirectory = new File(configurationDirectoy, TRANSFER_DIRECTORY);
//            
//            if (!transferlogDirectory.exists()) {
//                transferlogDirectory.mkdir();
//            }
//
//            initLoggers(configDirectory.getPath());

            initDB(configurationDirectoy.getPath());
            
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        
        
        
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                IDE.createAndShowIDE();
            }
        });

    }
    /**
     * To read database connection details from db.xml
     * @param configDirPath 
     */
    private static void initDB(String configDirPath) {
        File finalPath = new File(configDirPath, DB_CONFIG);
        if (finalPath.exists() && finalPath.canRead()) {
            FileInputStream fin = null;
            try {
                fin = new FileInputStream(finalPath);
                DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = builderFactory.newDocumentBuilder();
                Document xmlDocument = builder.parse(fin);
                XPath xPath = XPathFactory.newInstance().newXPath();
                String host = xPath.compile("/dbconfigure/host").evaluate(xmlDocument);
                String port = xPath.compile("/dbconfigure/port").evaluate(xmlDocument);
                String database = xPath.compile("/dbconfigure/database").evaluate(xmlDocument);
                String username = xPath.compile("/dbconfigure/username").evaluate(xmlDocument);
                String password = xPath.compile("/dbconfigure/password").evaluate(xmlDocument);
                DBConnection.setHost(host);
                DBConnection.setPort(port);
                DBConnection.setDbSchemaName(database);
                DBConnection.setDbLoginName(username);
                DBConnection.setDbLoginPwd(password);
            } catch (Exception ex) {
                ex.printStackTrace();
                Logger.getInstance().logMessage(ex);
            }
        }

    }
    /**
     * Method to populate main window
     */
    public static void createAndShowIDE() {
        frame = new JFrame(APPLICATION_NAME);
        try {
            // adding menu bar to ide
            menubar = new IDEMenubar();

            URL imgURL = IDE.class.getResource("/in/hcdc/cdac/resources/images/coedp_logo.png");
            frame.setIconImage(new ImageIcon(imgURL).getImage());

            //initLoggers(configDirectory.getPath());
            frame.setJMenuBar(menubar);
            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
            Dimension desiredDimension = new Dimension(dim.width, dim.height - 50);
            frame.setSize(desiredDimension);
           // setting location of IDE
            frame.setLocationRelativeTo(null);
            // adding contents to ide
            cardsHolder = new IDECardsHolder();
            frame.setContentPane(cardsHolder);
            frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            IDEEvent menuEventHandler = new IDEEvent(cardsHolder);
            menubar.getMenuItemExit().addActionListener(menuEventHandler);
            frame.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    int option = JOptionPane.showConfirmDialog(frame, "Are you sure you want to quit?", "Exit Dialog", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                    if (option == JOptionPane.YES_OPTION) {
                        // Helper.deleteConfigFile();
                        System.exit(0);
                    } else {
                    }
                }
            });
           frame.setVisible(true);
            
        } catch (Exception exception) {
            exception.printStackTrace();
            Logger.getInstance().logMessage(exception);
        }
    }
}
